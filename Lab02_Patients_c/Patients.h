#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef enum { FALSE, TRUE }BOOL;

typedef struct { char fname[15], lname[15]; unsigned int a, b, c; }PATIENT;
typedef struct  pqueue { PATIENT info; struct pqueue *next; }PQUEUE;

void read(PATIENT*);

BOOL enQueue/*Front*/(PQUEUE**, PATIENT*);
BOOL deQueue(PQUEUE**, PATIENT*);
void selectionSort(PQUEUE*);
void print(PQUEUE*);

void wait(unsigned int);