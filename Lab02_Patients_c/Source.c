#define _CRT_SECURE_NO_WARNINGS
#include <string.h>
#include <ctype.h>
#include "Patients.h"

int main()
{
	PQUEUE *head = NULL;
	unsigned short n;
	do printf("How many patients has arrived at ER? "), scanf("%hu", &n); while (n > 8 || n < 1);

	PATIENT p;
	for (unsigned short i = 0; i < n; ++i) {
		printf("%hu. PATIENT:\n", i + 1), read(&p);
		if (enQueue(&head, &p) == FALSE) exit(1);
	}

	selectionSort(head);
	system("CLS");
	printf("List of patients:\n"), print(head);

	char s[6];
	scanf("%s", s);
	for (unsigned i = 0; i < strlen(s); s[i] = tolower(s[i]), ++i);

	if (strcmp(s, "start") != 0) exit(1);
	system("CLS");
	printf("Start simulation!\n\n");
	while (deQueue(&head, &p))
	{
		printf("Patient %s %s is examined by doctor.\nStart time: ", p.fname, p.lname);
		time_t mytime = time(NULL);
		printf(ctime(&mytime));
		printf("Examination will take %d seconds\n", p.b);
		wait(p.b);
		printf("Examination of patient %s %s is over.\n\n", p.fname, p.lname);
	}

	system("PAUSE"), system("CLS");
	printf("End of simulation!");

	return 1;
}