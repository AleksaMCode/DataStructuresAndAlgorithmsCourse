#include "Patients.h"

void read(PATIENT *patient)
{
	printf("FIRST NAME: "), scanf("%s", patient->fname);
	printf("LAST NAME: "), scanf("%s", patient->lname);
	patient->a = rand() % 30 + 1;
	patient->b = rand() % 30 + 1;
	patient->c = rand() % 4 + 1;
}

BOOL enQueue(PQUEUE **pfront, PATIENT *data)
{
	PQUEUE *new = (PQUEUE*)malloc(sizeof(PQUEUE));
	if (!new) return FALSE;
	new->info = *data;
	new->next = NULL;
	if (*pfront == NULL || (*pfront)->info.a >= data->a) new->next = *pfront, *pfront = new;
	else {
		PQUEUE *tmp = *pfront;
		for (; tmp->next && tmp->info.a < data->a; tmp = tmp->next);
		new->next = tmp->next;
		tmp->next = new;
	}
	return TRUE;
}

BOOL deQueue(PQUEUE **pfront, PATIENT* p)
{
	if (*pfront == NULL) return FALSE;
	*p = (*pfront)->info;
	PQUEUE *tmp = *pfront;
	*pfront = (*pfront)->next;
	free(tmp);
	return TRUE;
}

void selectionSort(PQUEUE* head)
{
	for (; head->next && head; head = head->next) {
		PQUEUE *j, *max = head;
		for (j = head->next; j; j = j->next)
			if (j->info.c > max->info.c)  max = j;
		if (max != head) {
			PATIENT tmp = head->info;
			head->info = max->info;
			max->info = tmp;
		}
	}
}

void print(PQUEUE* head)
{
	if (head == NULL) return;
	printf("%s %s %d %d %d\n", head->info.fname, head->info.lname, head->info.a, head->info.b, head->info.c);
	print(head->next);
}

void wait(unsigned int sec)
{
	time_t returnt = time(0) + sec;
	while (time(0) < returnt);
}
